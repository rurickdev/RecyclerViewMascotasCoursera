# RecyclerViewMascotasCourseraConFragments

## Datos
Proyecto de la semana 4 del curso 3 de la especialidad Android.

El proyecto contiene 4 activities:

La primera es la MainActivity que contiene las 2 tabs, 1 con la lista de 19 mascotas diferentes y la 2 con el perfil de la mascota.
El segundo activity es MascotasFavoritasActivity que contiene una lista con 5 mascotas tomandas de MainActivity.
Ambas activities implementan el mismo objeto ObjMascota y el mismo adapter AdapterMascota para mostrar sus respectivos RecyclerView, ademas de tener su propio layout en el que se implementa una ActionBar personalizada para cada activity, tambien el icono del hueso dorado/amarillo permite aumentar el rating de la mascota.
La tercera es el area de contacto que permite enviar un e-mail con la libreria JavaMail.
La cuarta es la activity que muestra los datos del desarrollador.

Todas las Strings, dimens y colors estan en sus respectivos archivos dentro de [/res](/app/src/main/res/ "/res")

## Screenshots

![Alt text](/Screenshots/Screenshot01.png?raw=true "Screenshot 1")
![Alt text](/Screenshots/Screenshot02.png?raw=true "Screenshot 2")
![Alt text](/Screenshots/Screenshot03.png?raw=true "Screenshot 3")
![Alt text](/Screenshots/Screenshot04.png?raw=true "Screenshot 4")
![Alt text](/Screenshots/Screenshot05.png?raw=true "Screenshot 5")
![Alt text](/Screenshots/Screenshot06.png?raw=true "Screenshot 6")
![Alt text](/Screenshots/Screenshot07.png?raw=true "Screenshot 7")
![Alt text](/Screenshots/Screenshot08.png?raw=true "Screenshot 8")
![Alt text](/Screenshots/Screenshot09.png?raw=true "Screenshot 9")


## Datos tecnicos
Desarrollado con Android Studio 2.2.2

Testeado via ADB en un LG Nexus 5X con Android 7.0 Nougat 

## Desarrollador
Aplicacion desarrollada por Rurick M. Poisot a.k.a. @skintigth para Coursera
