package com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.adapters.AdapterMascota;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.pojo.ObjMascota;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.R;

import java.util.ArrayList;



public class RecyclerViewFragment extends Fragment {

    ArrayList<ObjMascota> listaMascotas;
    private RecyclerView fragmentRecyclerView;

    //Mascotas estaticas para poder ser usadas en otra clase/activity
    public static ObjMascota fido = new ObjMascota("Fido", R.drawable.dog_stock, 1);
    public static ObjMascota garfield = new ObjMascota("Garfield", R.drawable.cat_garfiled, 4);
    public static ObjMascota magestic = new ObjMascota("Majestic", R.drawable.dog_funny, 2);
    public static ObjMascota doggyBravo = new ObjMascota("Doggy Bravo", R.drawable.dog_glasses, 5);
    public static ObjMascota moonMoon = new ObjMascota("Moon Moon", R.drawable.dog_moon_moon, 6);
    static ObjMascota donGato = new ObjMascota("Don Gato", R.drawable.cat_funny, 3);
    static ObjMascota dog_1 = new ObjMascota("Majestic 2", R.drawable.dog_magestic_2, 6);
    static ObjMascota dog_2 = new ObjMascota("majestic 3", R.drawable.dog_magestic_af, 6);
    static ObjMascota dog_3 = new ObjMascota("Pug", R.drawable.dog_pug, 6);
    static ObjMascota dog_4 = new ObjMascota("Blaky", R.drawable.dog_say_hi, 6);
    static ObjMascota dog_5 = new ObjMascota("Sheep Dog", R.drawable.dog_sheep, 6);
    static ObjMascota dog_6 = new ObjMascota("Love Snow", R.drawable.dog_snow, 6);
    static ObjMascota dog_7 = new ObjMascota("What", R.drawable.dog_what, 6);
    static ObjMascota cat_2 = new ObjMascota("Big Eyes", R.drawable.cat_eyes, 6);
    static ObjMascota cat_3 = new ObjMascota("Fatty", R.drawable.cat_fat, 6);
    static ObjMascota cat_4 = new ObjMascota("Sorpresa", R.drawable.cat_sorpresed, 6);
    static ObjMascota cat_5 = new ObjMascota("Majestic", R.drawable.cat_magestic_af, 6);
    static ObjMascota cat_6 = new ObjMascota("Phone", R.drawable.cat_phone, 6);
    static ObjMascota cat_7 = new ObjMascota("CatTube", R.drawable.cat_tube, 6);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_recyclerview, container, false);

        //Creamos la lista que se mostrara con el RecyclerView
        //Debe existir la variable dentro de la clase MainActivity
        listaMascotas = new ArrayList<>();
        //Llenamos la lista con objetos nuevos
        llenarLista();

        //Asociamos el RecyclerView con su ID
        fragmentRecyclerView = (RecyclerView) v.findViewById(R.id.Fragment_Recyclerview);
        fragmentRecyclerView.setNestedScrollingEnabled(true);
        //Creamos un layout manager junto con su orientacion
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //Se le asosia dicho LayoutManager al RecyclerView
        fragmentRecyclerView.setLayoutManager(linearLayoutManager);
        //Para mandar la lista al ReyiclerView se necesita un adapter, en ese caso AdapterMascotas
        iniciarAdapter();

        return v;
    }

    //Metodo que inicia el Adapter
    public void iniciarAdapter(){
        AdapterMascota adaptador = new AdapterMascota(listaMascotas);
        fragmentRecyclerView.setAdapter(adaptador);
    }

    //Metodo encargado de llenar la lista
    public void llenarLista(){
        listaMascotas = new ArrayList<>();

        listaMascotas.add(fido);
        listaMascotas.add(garfield);
        listaMascotas.add(magestic);
        listaMascotas.add(doggyBravo);
        listaMascotas.add(donGato);
        listaMascotas.add(moonMoon);
        listaMascotas.add(dog_1);
        listaMascotas.add(dog_2);
        listaMascotas.add(dog_3);
        listaMascotas.add(dog_4);
        listaMascotas.add(dog_5);
        listaMascotas.add(dog_6);
        listaMascotas.add(dog_7);
        listaMascotas.add(cat_2);
        listaMascotas.add(cat_3);
        listaMascotas.add(cat_4);
        listaMascotas.add(cat_5);
        listaMascotas.add(cat_6);
        listaMascotas.add(cat_7);
    }
}
