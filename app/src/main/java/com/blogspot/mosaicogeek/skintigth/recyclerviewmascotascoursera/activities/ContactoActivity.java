package com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.activities;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.R;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.correo.EnviarCorreo;

public class ContactoActivity extends AppCompatActivity {

    private TextInputEditText correo, asunto, mensaje;
    public  static boolean enviado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_contacto_activity);

        //Agregar ActionBarPersonalizada
        Toolbar actionBar = (Toolbar) findViewById(R.id.ContactoActivity_ActionBar);
        //Cambia el texto del ActionBar
        actionBar.setTitle(R.string.support);
        setSupportActionBar(actionBar);
        //Mostrar boton Up en el activity
        try{
            //noinspection ConstantConditions
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch(Exception ignored){
        }

        correo = (TextInputEditText) findViewById(R.id.ContactoActivity_editText_correo);
        asunto = (TextInputEditText) findViewById(R.id.ContactoActivity_editText_asunto);
        mensaje = (TextInputEditText) findViewById(R.id.ContactoActivity_editText_mensaje);
    }

    //Metodo encargado de inflar el menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_action_bar_contacto_activity, menu);
        return true;
    }

    //Metodo que se encarga de las funciones de la ActionBar
    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){
            case R.id.menu_boton_enviar:
                enviarEmail();
                comprobarEnvio();
                break;
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //Metodo que muestra un snackBar segun fue o no enviado el mensaje
    public void snackBarConfirmacion(String mensaje){
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout)findViewById(R.id.ContactoActivity_CoordinatorLayout);
        Snackbar snackbarAbout = Snackbar.make(coordinatorLayout, mensaje, Snackbar.LENGTH_LONG);
        snackbarAbout.show();
    }

    //comprueba y muestra un toast si se envió o no el correo
    public void comprobarEnvio(){
        if(enviado){
            snackBarConfirmacion(getString(R.string.enviado));
        }else{
            snackBarConfirmacion(getString(R.string.no_enviado));
        }
    }

    //Metodo para enviar correo
    private void enviarEmail() {
        //Obtiene los datos de los EditText
        String email = correo.getText().toString().trim();
        String asunto2 = asunto.getText().toString().trim();
        String mensaje2 = mensaje.getText().toString().trim();

        //Crea el objeto EnviarCorreo
        EnviarCorreo cartero = new EnviarCorreo(this, email, asunto2, mensaje2);

        //Envia el email
        cartero.execute();
    }

    //Metodo que recupera la MainActivity al presionar el boton Back
    public void onBackPressed(){
        this.startActivity(new Intent(ContactoActivity.this, MainActivity.class));
    }
}
