package com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.pojo.ObjMascota;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.R;

import java.util.ArrayList;
import java.util.Locale;

public class AdapterMascota extends RecyclerView.Adapter<AdapterMascota.ViewHolderMascota>{

    //Lista de objetos mascota
    private ArrayList<ObjMascota> listaMascotas;

    //constructor
    public AdapterMascota(ArrayList<ObjMascota> listaMascotas){
        this.listaMascotas = listaMascotas;
    }

    //Infla el layout de la CardView
    @Override
    public ViewHolderMascota onCreateViewHolder(ViewGroup parent, int viewType) {
        //Copia el View que se va a repetir la cantidad de veces necesaria para la lista
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_pet_card, parent, false);
        //le envia al ViewHolder el View clonado para llenarlo con los elementos
        return new ViewHolderMascota(v);
    }

    //Le asigna los valores de la lista a los view que se reciclan
    @Override
    public void onBindViewHolder(ViewHolderMascota holder, int position) {
        //Objeto que clona al actual de la lista
        ObjMascota mascota = listaMascotas.get(position);
        //Se le indica al holder que use el recurso de la lista para llenar el view
        holder.fotoMascota.setImageResource(mascota.getFotoMascota());
        holder.nombreMascota.setText(mascota.getNombreMascota());
        //Se debe pasar el int a String para poder settear el texto
        holder.ratingMascota.setText(String.format(Locale.getDefault(), "%1$d", mascota.getRatingMascota()));
    }

    //Regresa la cantidad de elementos que tiene la lista
    @Override
    public int getItemCount() {
        return listaMascotas.size();
    }

    //Una clase secundaria necesaria para el adaptador
    static class ViewHolderMascota extends RecyclerView.ViewHolder{

        //Views que se van a reciclar
        private ImageView fotoMascota;
        private TextView nombreMascota;
        private TextView ratingMascota;
        private ImageView huesoDorado;

        //Este constructor es el que relaciona los componentes
        //de los objetos con los elementos del layout
        ViewHolderMascota(final View itemView) {
            super(itemView);
            //Relacionamos los Views a reciclar con los respectivos ID de la tarjeta
            fotoMascota = (ImageView) itemView.findViewById(R.id.Cardview_ImagenMascota);
            nombreMascota = (TextView) itemView.findViewById(R.id.Cardview_NombreMascota);
            ratingMascota = (TextView) itemView.findViewById(R.id.Cardview_ratingMascota);
            huesoDorado = (ImageView) itemView.findViewById(R.id.Cardview_HuesoDorado);

            //se le asigna un OnClickListener al botón dorado para agregar rating
            huesoDorado.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CharSequence rating = ratingMascota.getText();
                    int contadorRating = Integer.parseInt(String.valueOf(rating));
                    //suma 1 al rating cuando se preciona
                    contadorRating++;

                    ratingMascota.setText(String.format(Locale.getDefault(), "%1$d", contadorRating));
                }
            });
        }
    }

}
