package com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.correo;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.activities.ContactoActivity;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

//Estos metodos se tomaron del tutorial
//https://www.simplifiedcoding.net/android-email-app-using-javamail-api-in-android-studio/
public class EnviarCorreo extends AsyncTask<Void,Void,Void> {

    private Context contexto;
    private String email;
    private String asunto;
    private String mensaje;

    //Mensaje de enviando
    private ProgressDialog progressDialog;

    //Constructor
    public EnviarCorreo(Context contexto, String email, String asunto, String mensaje){
        this.contexto = contexto;
        this.email = email;
        this.asunto = asunto;
        this.mensaje = mensaje;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //Muestra dialogo de progreso al enviar e-mail
        progressDialog = ProgressDialog.show(contexto,"Enviando mensaje","Por favor espere...",false,false);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        //Borra el dialogo de progreso
        progressDialog.dismiss();
        //Muestra un mensaje de envio exitoso
    }

    @Override
    protected Void doInBackground(Void... params) {
        boolean enviado;

        Session sesion;
        //Crea el objeto propiedades
        Properties props = new Properties();

        //Configura propiedades para Gmail
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        //Creando una nueva sesion
        sesion = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    //Autenticacion de la cuenta
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(Configuracion.EMAIL, Configuracion.PASSWORD);
                    }
                });

        try {
            //Crea el objeto MineMessage
            MimeMessage mm = new MimeMessage(sesion);

            //Configurando la cuenta del remitente
            mm.setFrom(new InternetAddress(Configuracion.EMAIL));
            //Agregando receptor
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress("skintigthvader@gmail.com"));
            //agregando asunto
            mm.setSubject(asunto);
            //Agregando mensaje
            mm.setText("E-Mail: "+email+"\nMensaje: "+mensaje);

            //enviando e-mail
            Transport.send(mm);
            ContactoActivity.enviado=true;

        } catch (MessagingException e) {
            ContactoActivity.enviado=false;
            e.printStackTrace();
        }
        return null;
    }
}