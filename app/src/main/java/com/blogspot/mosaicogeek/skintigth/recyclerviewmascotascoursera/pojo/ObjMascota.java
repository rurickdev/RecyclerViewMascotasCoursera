package com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.pojo;


public class ObjMascota {

    //Carcateristicas del objeto
    private int fotoMascota;
    private String nombreMascota;
    private int ratingMascota;

    //Constructor para el objeto
    public ObjMascota(String nombreMascota, int fotoMascota, int ratingMascota) {
        this.fotoMascota = fotoMascota;
        this.nombreMascota = nombreMascota;
        this.ratingMascota = ratingMascota;
    }

    public int getFotoMascota() {
        return fotoMascota;
    }

    public String getNombreMascota() {
        return nombreMascota;
    }

    public int getRatingMascota() {
        return ratingMascota;
    }

}
