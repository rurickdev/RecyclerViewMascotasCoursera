package com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.activities;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.R;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.adapters.PageAdapter;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.fragments.PerfilFragment;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.fragments.RecyclerViewFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main_activity);

        //Agrega la ActionBar personalizada
        toolbar = (Toolbar) findViewById(R.id.MainActivity_ToolBar);
        tabLayout = (TabLayout) findViewById(R.id.MainActivity_TabLayout);
        viewPager = (ViewPager) findViewById(R.id.MainActivity_ViewPager);

        //Validacion para el toolbar
        if (toolbar != null){
            //cambia el texto del ActionBar
            toolbar.setTitle(R.string.nombre_proyecto);
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorTextIcons));
            //Cambiar el icono al lado del texto
            toolbar.setNavigationIcon(R.drawable.ic_huella);
            setSupportActionBar(toolbar);
        }

        setUpViewPager();
    }

    //Agrega los fragments a una lista que se pasará el SetUpViewPager
    private ArrayList<Fragment> agregarFragment(){
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new RecyclerViewFragment());
        fragments.add(new PerfilFragment());
        return fragments;
    }

    //Metodo que configura el ViewPager con los fragments
    private void setUpViewPager(){
        viewPager.setAdapter(new PageAdapter(getSupportFragmentManager(), agregarFragment()));
        tabLayout.setupWithViewPager(viewPager);
        //Agrega los iconos a las Tabs
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_pet_home);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_moon_moon);
    }

    //Metodo que crea el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_action_bar_main_activity, menu);
        return true;
    }

    //Metodo que define que hacer segun se seleccione un elemnto del menú
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout)findViewById(R.id.MainActivity_CoordinatorLayout);

        switch(item.getItemId()){
            case R.id.menu_starred_pets:
                Intent intent = new Intent(this, MascotasFavoritasActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.menu_about:
                Intent intent2 = new Intent(this, AboutActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.menu_contacto:
                Intent intent3 = new Intent(this, ContactoActivity.class);
                startActivity(intent3);
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
